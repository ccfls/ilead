import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

GPIO.setup(12,GPIO.IN)

off = True

while True:
    if (GPIO.input(12) and off):
        print("Button Pressed")
        off = False
    elif (not GPIO.input(12) and not off):
        off = True

